FROM ruby:stretch

RUN gem install markdown2confluence && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y -qq \
      bzip2 \
      texlive-full \
      ttf-liberation \
      jq \
      libfontconfig \
      libxrender1 \
      libxtst6 \
      wget \
      xz-utils \
      rsync \
      libssl1.1 \
      openssl \
      fontconfig \
      ca-certificates \
      libfontenc1 \
      libxfont1 \
      xfonts-encodings \
      xfonts-utils \
      xfonts-75dpi \
      xfonts-base

RUN wget https://api.github.com/repos/jgm/pandoc/releases/latest --no-check-certificate -O - | \
      jq -r '.assets[] | select(.name | index("amd64.deb")) | .browser_download_url' | \
      wget --no-check-certificate -i - --quiet -O pandoc.deb && \
    dpkg -i pandoc.deb && \
    wget https://api.github.com/repos/wkhtmltopdf/wkhtmltopdf/releases/latest --no-check-certificate -O - | \
      jq -r '.assets[] | select(.name | index("stretch_amd64.deb")) | .browser_download_url' | \
      wget --no-check-certificate -i - --quiet -O wkhtmltox.deb && \
    dpkg -i wkhtmltox.deb && \
    rm -f pandoc.deb wkhtmltox.deb
